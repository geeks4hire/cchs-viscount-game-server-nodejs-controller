/*
 * mqtt_service.js: Handler for MQTT communications sent from microcontrollers
 * and Android simulator control app - maintains state of various values and
 * parameters needed for the simulation, and for inter-device synchronicity.
 *
 * Component of Connected Community HackerSpace "Viscountess Space Simulator"
 * system. Visit http://www.hackmelb.org.au/ for more information.
 *
 * (C) Copyright 2014, Ben Horan <benh@geeksforhire.com.au>
 *
 */

var mqtt = require('mqtt');
var async = require('async');

var MQTTService = exports.MQTTService = function (options) {
    this.options = options ? options : {};
    console.log("mqtt_service.js Instantiated...");
};

MQTTService.prototype.start = function(callback) {
    console.log("mqtt_service.js Starting...");
    callback(this);
}

MQTTService.prototype.stop = function(callback) {
    console.log("mqtt_service.js Stopping...");
    callback(this);
}