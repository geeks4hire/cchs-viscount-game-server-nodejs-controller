/*
 * start-server.js: Wrapper for spawning server.js as long-running daemon
 *
 * Component of Connected Community HackerSpace "Viscountess Space Simulator"
 * system. Visit http://www.hackmelb.org.au/ for more information.
 *
 * (C) Copyright 2014, Ben Horan <benh@geeksforhire.com.au>
 * 
 */
 
var http = require('http');
var server = require('apiserver');
var async = require('async');
 
 var RESTService = exports.RESTService = function (options) {
     options || (options = {});
     console.log("rest_service.js Instantiated...");
 };
 