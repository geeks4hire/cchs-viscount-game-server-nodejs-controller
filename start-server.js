/*
 * start-server.js: Wrapper for spawning server.js as long-running daemon
 *
 * Component of Connected Community HackerSpace "Viscountess Space Simulator"
 * system. Visit http://www.hackmelb.org.au/ for more information.
 *
 * (C) Copyright 2014, Ben Horan <benh@geeksforhire.com.au>
 * 
 */
 
var path = require('path'),
    forever = require('forever');

var monitor = forever.startDaemon(path.join(__dirname, 'server.js'));

console.log("Now spawning 'server.js' as daemon (it will be re-started if killed)...");    

monitor.on('start', function () {
  forever.startServer(monitor);
});
