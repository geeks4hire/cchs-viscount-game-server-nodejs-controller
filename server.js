/*
 * server.js: Main server script to provide MQTT messaging processing, state
 * persistence of simulator status and values, mDNS ZeroConf service
 * advertisement, and communications with the micro's and UI's involved in
 * the simulator (including Processing and Unity3D).
 *
 * Component of Connected Community HackerSpace "Viscountess Space Simulator"
 * system. Visit http://www.hackmelbourne.org.au/ for more information.
 *
 * (C) Copyright 2014, Ben Horan <benh@geeksforhire.com.au>
 *
 */

var http = require('http');
var path = require('path');
var async = require('async');

var mqttModule = require(path.resolve(__dirname, 'lib', 'mqtt_service'));
var restApiModule = require(path.resolve(__dirname,'lib', 'rest_service'));

mqttModule.MQTTService();
restApiModule.RESTService();